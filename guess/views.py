from django.shortcuts import render
from django.http import HttpResponse
import random
# Create your views here.

def home(request):
    return render(request,'guess/home.html')

def result(request):
    numbers=list('1234567890')
    length=2
    p=''
    for x in range(length):
        p+=random.choice(numbers)
    p=int(p)
    num=int(request.GET.get('num'))
    if num==p:
        res=" You Win !!! "
    elif num<p:
        p=str(p)
        res=" Guess higher "
    else:
        p=str(p)
        res=" Guess lower "
       

    return render(request,'guess/result.html',{'result':res,'p':p})
